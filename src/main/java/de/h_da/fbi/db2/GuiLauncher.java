package de.h_da.fbi.db2;

import javafx.application.Application;
import javafx.stage.Stage;

public class GuiLauncher extends Application {

   public static void main(String[] args) {
      System.out.println("Application started ...");
      launch(args);
   }

   @Override
   public void start(Stage primaryStage) {

   }
}
