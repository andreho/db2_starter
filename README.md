How-to install and to use Gradle-Plugin for Net-Beans: 

 - Please watch the original video guide at [YouTube](https://www.youtube.com/watch?v=2EIUHHoVfmU)
 
Do I need to install Gradle at my local machine:

- No, you can use the delivered Gradle-Wrapper to work with Gradle. 
- To do so, just use `gradlew` in your console from your project folder. 
 
How-to edit dependencies of a project:

 - Please modify your `build.gradle` file at the `dependencies`-section
 
Where can be more libraries found:
 
 - Please visit [Maven-Central-Repository](http://search.maven.org/) to find more libraries